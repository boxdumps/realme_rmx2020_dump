## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1607914664672 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: arrow_RMX2020-userdebug
- Release Version: 12
- Id: SQ1D.211205.017
- Incremental: eng.neolit.20220109.125950
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SQ1A.220105.002/7961164:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1607914664672-release-keys-random-text-191323211714466
- Repo: realme_rmx2020_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
